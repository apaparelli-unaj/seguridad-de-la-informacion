import os
import subprocess, signal

import dbus
from dbus.mainloop.glib import DBusGMainLoop


PENKEY = '/Org/Freedesktop/Udisks2/Drives/Kingston_Datatraveler_2_2E0_0910D25130A018C0'

DBusGMainLoop(set_as_default=True)
bus = dbus.SystemBus()


def callback_function_remove(*args):
    if args[0].title() == PENKEY:
         pid = int(subprocess.check_output(["pidof", 'xtrlock']))
        os.kill(pid, signal.SIGKILL)


iface  = 'org.freedesktop.DBus.ObjectManager'
signal_remove = 'InterfacesAdded'

bus.add_signal_receiver(callback_function_remove, signal_remove, iface)

import gobject
loop = gobject.MainLoop()
loop.run()
