

import dbus
#DRIVER = '/org/freedesktop/UDisks2/drives/Kingston_DataTraveler_2_2e0_0910D25130A018CO'
PENKEY = '/Org/Freedesktop/Udisks2/Drives/Kingston_Datatraveler_2_2E0_0910D25130A018C0'

#bus = dbus.SystemBus()
#obj = bus.get_object('org.freedesktop.UDisks2', DRIVER)
#iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties') # Here we use this 'magic' interface
#iface.GetAll('org.freedesktop.UDisks2.Drive')

#import dbus
from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

bus = dbus.SystemBus()

import os
import subprocess, signal


def callback_function_added(*args):
    if args[0].title() == PENKEY:
        pid = int(subprocess.check_output(["pidof", 'xtrlock']))
        os.kill(pid, signal.SIGKILL)


def callback_function_remove(*args):
    print("BYE")
    #if args[0].title() == PENKEY:
        # pid = int(subprocess.check_output(["pidof", 'xtrlock']))
        #os.kill(pid, signal.SIGKILL)


iface  = 'org.freedesktop.DBus.ObjectManager'
signal_added = 'InterfacesAdded'
signal_remove = 'InterfacesAdded'

bus.add_signal_receiver(callback_function_added, signal_added, iface)
bus.add_signal_receiver(callback_function_remove, signal_remove, iface)

import gobject
loop = gobject.MainLoop()
loop.run()
